import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { Feature1Component } from './feature1/feature1.component';
import { F1NewComponent } from './feature1/f1-new/f1-new.component';
import { F1DetailsComponent } from './feature1/f1-details/f1-details.component';
import { Feature2Component } from './feature2/feature2.component';
import { F2NewComponent } from './feature2/f2-new/f2-new.component';
import { MenuComponent } from './menu/menu.component';
import { BreadCrumbComponent } from './bread-crumb/bread-crumb.component';
import { F1DetailsTipoComponent } from './feature1/f1-details/f1-details-tipo/f1-details-tipo.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    Feature1Component,
    F1NewComponent,
    F1DetailsComponent,
    Feature2Component,
    F2NewComponent,
    MenuComponent,
    BreadCrumbComponent,
    F1DetailsTipoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
