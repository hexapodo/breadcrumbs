import { ComponentFixture, TestBed } from '@angular/core/testing';

import { F1DetailsTipoComponent } from './f1-details-tipo.component';

describe('F1DetailsTipoComponent', () => {
  let component: F1DetailsTipoComponent;
  let fixture: ComponentFixture<F1DetailsTipoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ F1DetailsTipoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(F1DetailsTipoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
