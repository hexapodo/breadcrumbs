import { ComponentFixture, TestBed } from '@angular/core/testing';

import { F1NewComponent } from './f1-new.component';

describe('F1NewComponent', () => {
  let component: F1NewComponent;
  let fixture: ComponentFixture<F1NewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ F1NewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(F1NewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
