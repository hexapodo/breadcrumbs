import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, Routes } from '@angular/router';
import { filter, tap } from 'rxjs';

@Component({
  selector: 'app-bread-crumb',
  templateUrl: './bread-crumb.component.html',
  styleUrls: ['./bread-crumb.component.scss']
})
export class BreadCrumbComponent implements OnInit {

  elementos: NavigationItem = {
    url: '/home',
    label: 'Home',
    items: [
      {
        url: '/feature1',
        label: 'Feature 1',
        items: [
          {
            url: '/feature1/new',
            label: 'New F1'
          },
          {
            url: '/feature1/details/:id',
            label: 'Details F1',
            items: [
              {
                url: '/feature1/details/porTipo/:tipo',
                label: 'Por tipo'
              }
            ]
          }
        ]
      },
      {
        url: '/feature2',
        label: 'Feature 2',
        items: [
          {
            url: '/feature2/new',
            label: 'New F2'
          }
        ]
      }
    ]
  }
  nav: any;

  navBreadCrumbs = new CompNI(this.elementos)

  constructor(
    private router: Router,
  ) {
    router.events.pipe(
      filter((event) => {
        return event instanceof NavigationEnd
      }),
      // tap({next: (event) => {
      //   console.log(this.elementos);
      // }})
    ).subscribe(
      (event)=>{
        const activeRoute = (event as NavigationEnd).urlAfterRedirects;
        this.nav = this.navBreadCrumbs.getNavigation(activeRoute).reverse()
      }
    );
  }

  ngOnInit(): void {
  }

}

export interface NavigationItem {
  url: string;
  label: string;
  items?: NavigationItem[];
}

class CompNI {
  parent: CompNI | null = null;
  items: CompNI[] = [];
  item: NavigationItem | null = null;

  constructor(item: NavigationItem) {
    this.item = item;
    item.items?.forEach(
      item => {
        const newItem = new CompNI(item);
        newItem.setParent(this);
        this.add(newItem);
      }
    )
  }

  add(item: CompNI) {
    this.items.push(item);
  }

  setParent(parent: CompNI): void {
    this.parent = parent;
  }

  getNavigation(ruta: string, items: (NavigationItem |null)[] = []) {
    this.items.forEach(
      item => {
        item.getNavigation(ruta, items)
      }
    )
    if ((this.item?.url === ruta) || this.compare(this.item?.url ? this.item.url : '', ruta)) {
      let parent = this.parent
      while (parent) {
        items.push(parent.item)
        parent = parent.parent
      }
    }
    return items
  }

  private compare(configuredUrl: string, activeUrl:string):boolean {
    let matched = true;
    const configuredUrlSplited =  configuredUrl.split('/');
    const activeUrlSplited = activeUrl.split('/');
    activeUrlSplited.forEach((item, idx) => {
      if (configuredUrlSplited.length === activeUrlSplited.length) {
        matched &&= item === configuredUrlSplited[idx] || configuredUrlSplited[idx].startsWith(':');
      } else {
        matched = false;
      }
    })
    return matched;
  }
}

