import { ComponentFixture, TestBed } from '@angular/core/testing';

import { F2NewComponent } from './f2-new.component';

describe('F2NewComponent', () => {
  let component: F2NewComponent;
  let fixture: ComponentFixture<F2NewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ F2NewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(F2NewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
