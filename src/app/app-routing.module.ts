import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { F1DetailsTipoComponent } from './feature1/f1-details/f1-details-tipo/f1-details-tipo.component';
import { F1DetailsComponent } from './feature1/f1-details/f1-details.component';
import { F1NewComponent } from './feature1/f1-new/f1-new.component';
import { Feature1Component } from './feature1/feature1.component';
import { F2NewComponent } from './feature2/f2-new/f2-new.component';
import { Feature2Component } from './feature2/feature2.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'home', pathMatch: 'full'
  },
  {
    path: 'home', component: HomeComponent
  },
  {
    path: 'feature1', component: Feature1Component
  },
  {
    path: 'feature1/new', component: F1NewComponent
  },
  {
    path: 'feature1/details/:id', component: F1DetailsComponent
  },
  {
    path: 'feature1/details/porTipo/:tipo', component: F1DetailsTipoComponent
  },
  {
    path: 'feature2', component: Feature2Component
  },
  {
    path: 'feature2/new', component: F2NewComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
